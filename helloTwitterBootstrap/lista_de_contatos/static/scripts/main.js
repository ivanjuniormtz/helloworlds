(function () {
    var Contato, Formulario, formulario;

    Contato = (function() {
        var Mapper;

        function confirmar_exclusao(mapper, contato) {
            var confirmacao = $('#excluir-contato');

            confirmacao.find('#nome-do-contato').text(contato.nome);

            confirmacao.find('#confirmar-remocao').unbind().click(function() {
                mapper.remove(contato.id);
                confirmacao.modal('hide');
            });

            confirmacao.find('#cancelar-remocao').unbind().click(function() {
                confirmacao.modal('hide');
            });

            confirmacao.modal();
        }

        function renderizar(mapper, contato) {
            var saida, acoes;

            saida = $('<tr></tr>');
            $('<td></td>').text(contato.nome).appendTo(saida);
            $('<td></td>').text(contato.email).appendTo(saida);
            $('<td></td>').text(contato.telefone).appendTo(saida);

            acoes = $('<td></td>')
            $('<span class="acao" title="Editar"><i class="icon-pencil"></i></span>').click(function() {
                formulario.editar_contato(contato);
            }).appendTo(acoes);
            $('<span class="acao" title="Apagar"><i class="icon-trash"></i></span>').click(function() {
                confirmar_exclusao(mapper, contato);
            }).appendTo(acoes);
            saida.append(acoes);

            return saida;
        }

        Mapper = (function() {
            var handle_item;

            handle_item = function(item, id, handler) {
                var contatos; 
                contatos = $.parseJSON(this.storage.getItem(this.key));
                if (typeof item === 'undefined' || item === null) {
                    item = $(contatos).filter(function(i){ 
                        return contatos[i].id === id;
                    })[0];
                }
                handler.apply(this, [contatos, item]);
                this.storage.setItem(this.key, $.toJSON(contatos));
                this.load();
                return item;
            }

            function Mapper(storage) {
                if (typeof storage === 'undefined' || storage === null) {
                    storage = window.localStorage;
                }
                this.storage = storage;
            };

            Mapper.prototype.key = 'LISTA_DE_CONTATOS.contato';
            
            Mapper.prototype.add = function(item) {
                return handle_item.apply(this, [item, null, function(list, item) {
                    var max; 
                    if (!$.isNumeric(item.id)) {
                        max = Math.max.apply(null,
                         $.map(list, function(i) { 
                             return Number(i.id); 
                         }));

                        item.id = $.isNumeric(max) ? max + 1 : 1;
                    }
                    list.push(item);
                }]);
            }; 
            
            Mapper.prototype.remove = function(id) {
                return handle_item.apply(this, [null, id, function(list, item) {
                    var index; 
                    index = $.inArray(item, list);
                    if (index === -1) {
                         return;
                    }
                    list.splice(index, 1);
                }]);
            }; 


            Mapper.prototype.load = function() {
                var that = this, contatos, corpo;

                contatos = $.parseJSON(this.storage.getItem(this.key));

                if (typeof contatos === 'undefined' || contatos === null) {
                    contatos = [];
                    this.storage.setItem(this.key, $.toJSON(contatos));
                }

                // NOTE: Ordem alfabética
                contatos.sort(function(an, other) {
                    return an.nome > other.nome;
                });

                corpo = $('#tabela-de-contatos tbody');
                corpo.empty();
                
                $.each(contatos, function(index, contato) {
                    corpo.append(renderizar(that, contato));
                });
            };

            return Mapper;
        })();

        function Contato(dados) {
            this.id = dados.id || null;
            this.nome = dados.nome;
            this.email = dados.email;
            this.telefone = dados.telefone;
        }

        Contato.objects = new Mapper();

        Contato.prototype.save = function() {
            if ($.isNumeric(this.id)) {
                this.delete_();
            }

            return Contato.objects.add(this);
        };

        Contato.prototype.delete_ = function() {
            return Contato.objects.remove(this.id);
        };
        
        return Contato;
    })();

    Formulario = (function() {
        var aplicar_bind, alterar_botao_adicionar, alterar_botao_salvar;
        
        aplicar_bind = function(){ 
            var that = this;
            this.html.submit(function(e) {
                that.obter_contato().save();
                that.limpar();

                e.preventDefault();
                e.stopPropagation()
                return false;
            });
        };

        alterar_botao_adicionar = function() {
            this.html.find('#botao-confirmar')
             .removeClass('btn-warning')
             .text('Adicionar');
        };

        alterar_botao_salvar = function() {
            this.html.find('#botao-confirmar')
             .addClass('btn-warning')
             .text('Salvar');
        };


        function Formulario(html) {
            this.html = html;
            this.fields = {
                'nome': html.find('#nome_id'),
                'email': html.find('#email_id'),
                'telefone': html.find('#telefone_id')
            };
            this.contato = null;
            aplicar_bind.apply(this);
            alterar_botao_adicionar.apply(this);
        }

        Formulario.prototype.obter_contato = function() {
            return new Contato({
                'id': this.contato === null ? null : this.contato.id,
                'nome': this.fields.nome.val(),
                'email': this.fields.email.val(),
                'telefone': this.fields.telefone.val()
            });
        };

        Formulario.prototype.editar_contato = function(contato) {
            this.fields.nome.val(contato.nome);
            this.fields.email.val(contato.email);
            this.fields.telefone.val(contato.telefone);
            this.contato = contato;
            alterar_botao_salvar.apply(this);
        };

        Formulario.prototype.limpar = function() {
            this.contato = null;
            alterar_botao_adicionar.apply(this);
            return $.each(this.fields, function(i, elemento) { elemento.val(''); });
        };

        return Formulario;
    })();

    $(function() {
        formulario = new Formulario($('#formulario-contatos'));
        $('#telefone_id').mask('(99) 9999-9999');
        Contato.objects.load();
    });
})();
